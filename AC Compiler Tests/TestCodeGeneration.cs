using System;
using System.IO;
using ACCompiler;
using ACCompiler.Util;
using NUnit.Framework;

namespace Tests {
    [TestFixture]
    public class TestCodeGeneration {
        private static readonly string[] Examples = {
            /* 0 */ "float b   int a   a = 5   b = a + 3.2   p b",
            /* 1 */ "float b   int a   a = 5   b = a - 3.2   p b",
            /* 2 */ "float b   int a   a = 5   b = a * 3.2   p b",
            /* 3 */ "i d d = 3.2",
            /* 4  */ "a a",
            /* 5 */ "f f",
            /* 6  */ "a = 5  f b",
            /* 7 */ "% %",
            /* 8 */ "float b   int a   a = 5   b = a / 3.2   p b",
            /* 9 */ "float b   int a   a = 5   b = a % 3.2   p b",
            /* 10 */ "float b   int a   a = 5   b = 3.2 + a ^ a   p b",
        };

        [Test]
        public void TestExample0Adding() {
            Assert.AreEqual(Compiler.Compile(Examples[0]), " 5  sa 0 k la  3.2 + sb 0 k lb p si ");
        }

        [Test]
        public void TestExample1Subtracting() {
            Assert.AreEqual(Compiler.Compile(Examples[1]), " 5  sa 0 k la  3.2 - sb 0 k lb p si ");
        }

        [Test]
        public void TestExample2Multiply() {
            Assert.AreEqual(Compiler.Compile(Examples[2]), " 5  sa 0 k la  3.2 * sb 0 k lb p si ");
        }

        [Test]
        public void TestExample8Divide() {
            Assert.AreEqual(Compiler.Compile(Examples[8]), " 5  sa 0 k la  3.2 / sb 0 k lb p si ");
        }

        [Test]
        public void TestExample9Modulo() {
            Assert.AreEqual(Compiler.Compile(Examples[9]), " 5  sa 0 k la  3.2 % sb 0 k lb p si ");
        }

        [Test]
        public void TestExample10Exponent() {
            Assert.AreEqual(Compiler.Compile(Examples[10]), " 5  sa 0 k  3.2 la la ^ 5 k + sb 0 k lb p si ");
        }

        [Test]
        public void TestExample3() {
            Assert.AreEqual(Compiler.Compile(Examples[3]), " 3.2  sd 0 k ");
        }

        [Test]
        public void TestExample4() {
            Parser parser =
                new Parser(new CharStream(new StreamReader(Compiler.GenerateStreamFromString(Examples[4]))));
            try {
                parser.Program();
            } catch (Exception) {
                Assert.Pass();
                return;
            }

            Assert.Fail();
        }

        [Test]
        public void TestExample5() {
            Parser parser =
                new Parser(new CharStream(new StreamReader(Compiler.GenerateStreamFromString(Examples[5]))));
            try {
                parser.Program();
            } catch (Exception) {
                Assert.Pass();
                return;
            }

            Assert.Fail();
        }

        [Test]
        public void TestExample6() {
            Parser parser =
                new Parser(new CharStream(new StreamReader(Compiler.GenerateStreamFromString(Examples[6]))));
            try {
                parser.Program();
            } catch (Exception) {
                Assert.Pass();
                return;
            }

            Assert.Fail();
        }

        [Test]
        public void TestParserFail() {
            try {
                Compiler.Compile(Examples[7]);
            } catch (Exception) {
                Assert.Pass();
                return;
            }

            Assert.Fail();
        }

        [Test]
        public void TestCompile() {
            Assert.AreEqual(Compiler.Compile("float b   int a   a = 5   b = a + 3.2   p b"),
                " 5  sa 0 k la  3.2 + sb 0 k lb p si ");
        }
    }
}