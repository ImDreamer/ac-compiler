using System.Collections.Generic;

namespace ACCompiler.Types {
    public class Program : AbstractSyntaxTree {
        public readonly List<AbstractSyntaxTree> Programs;

        public Program(List<AbstractSyntaxTree> prg) {
            Programs = prg;
        }


        public override void PrettyPrint() {
            foreach (AbstractSyntaxTree ast in Programs) {
                ast.PrettyPrint();
            }
        }

        public override void SymbolTableFilling() {
            foreach (AbstractSyntaxTree ast in Programs) {
                ast.SymbolTableFilling();
            }
        }

        public override void TypeChecking() {
            foreach (AbstractSyntaxTree ast in Programs) {
                ast.TypeChecking();
            }
        }

        public override void CodeGeneration() {
            foreach (AbstractSyntaxTree ast in Programs) {
                ast.CodeGeneration();
            }
        }
    }
}