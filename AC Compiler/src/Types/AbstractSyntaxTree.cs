using System;
using System.Collections.Generic;
using ACCompiler.Casting;

namespace ACCompiler.Types {
    public abstract class AbstractSyntaxTree {
        public static readonly Dictionary<string, Constants> SymbolTable = new Dictionary<string, Constants>();
        public static string Code = "";
        public Constants Type;

        public abstract void PrettyPrint();

        public abstract void SymbolTableFilling();

        public abstract void TypeChecking();

        protected static void Error(string message) {
            throw new Exception(message);
        }

        protected static Constants Generalize(Constants t1, Constants t2) {
            if (t1 == Constants.FNUM || t2 == Constants.FNUM) return Constants.FNUM;
            return Constants.INUM;
        }

        protected static AbstractSyntaxTree Convert(AbstractSyntaxTree n, Constants t) {
            switch (n.Type) {
                case Constants.FNUM when t == Constants.INUM:
                    Error("Illegal type conversion");
                    break;
                case Constants.INUM when t == Constants.FNUM:
                    return new ConvertToFloat(n);
            }

            return n;
        }

        public abstract void CodeGeneration();

        protected static void Emit(string c) {
            Code = Code + c;
        }
    }
}