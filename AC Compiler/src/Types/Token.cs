namespace ACCompiler.Types {
    public class Token {
        public readonly Constants Type;
        public readonly string Value;

        public Token(Constants type, string value = "") {
            Type = type;
            Value = value;
        }

        public override string ToString() {
            return "Type:\t" + Type + "\tValue:\t" + Value;
        }
    }
}