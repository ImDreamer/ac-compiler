using System;
using System.Collections.Generic;
using ACCompiler.Declarations;
using ACCompiler.Operations;
using ACCompiler.Types;
using ACCompiler.Util;


//The construction of the AST is a little messy as the grammar for the ac language is Expr -> (+|-) Val Expr
//which will be used in the Stm -> Id assign Val Expr production. However, we really want the AST
//to have an Assigning node corresponding to Id assign Expr where Expr -> Val (+|-) Expr i.e. a Computing node
//thus we create a Computing node in this parse method with an empty left child and 
//in the parse method for STM we adjust the AST with the correct left child


namespace ACCompiler {
    public class Parser {
        private readonly TokenStream _ts;

        public Parser(CharStream s) {
            _ts = new TokenStream(s);
        }


        public AbstractSyntaxTree Program() {
            Program itsAst = new Program(new List<AbstractSyntaxTree>());
            if (_ts.Peek() == Constants.FLOATDCL || _ts.Peek() == Constants.INTDCL || _ts.Peek() == Constants.ID ||
                _ts.Peek() == Constants.PRINT || _ts.Peek() == Constants.EOF) {
                var declarations = Declarations();
                var statements = Statements();
                Expect(Constants.EOF);
                if (declarations != null) itsAst.Programs.AddRange(declarations);
                if (statements != null) itsAst.Programs.AddRange(statements);
            } else Error("Expected float dcl, int dcl, id, print, or eof");

            return itsAst;
        }

        private List<AbstractSyntaxTree> Declarations() {
            var abstractSyntaxTrees = new List<AbstractSyntaxTree>();
            if (_ts.Peek() == Constants.FLOATDCL || _ts.Peek() == Constants.INTDCL) {
                AbstractSyntaxTree declaration = Declaration();
                var declarations = Declarations();
                abstractSyntaxTrees.Add(declaration);
                abstractSyntaxTrees.AddRange(declarations);
            } else if (_ts.Peek() == Constants.ID || _ts.Peek() == Constants.PRINT || _ts.Peek() == Constants.EOF) {
                // Do nothing for lambda-production
            } else Error("Expected float dcl, int dcl, id, print, or eof");

            return abstractSyntaxTrees;
        }

        private AbstractSyntaxTree Declaration() {
            AbstractSyntaxTree itsAbstractSyntaxTree = null;
            if (_ts.Peek() == Constants.FLOATDCL) {
                Expect(Constants.FLOATDCL);
                itsAbstractSyntaxTree = new FloatDcl(Expect(Constants.ID).Value);
            } else if (_ts.Peek() == Constants.INTDCL) {
                Expect(Constants.INTDCL);
                Token t = Expect(Constants.ID);
                itsAbstractSyntaxTree = new IntDcl(t.Value);
            } else Error("Expected float or int declaration");

            return itsAbstractSyntaxTree;
        }


        private List<AbstractSyntaxTree> Statements() {
            var abstractSyntaxTrees = new List<AbstractSyntaxTree>();
            if (_ts.Peek() == Constants.ID || _ts.Peek() == Constants.PRINT) {
                AbstractSyntaxTree stmt = Statement();
                var statements = Statements();
                abstractSyntaxTrees.Add(stmt);
                abstractSyntaxTrees.AddRange(statements);
            } else if (_ts.Peek() == Constants.EOF) {
                // Do nothing for lambda-production
            } else Error("expected id, print, or eof");

            return abstractSyntaxTrees;
        }

        private AbstractSyntaxTree Statement() {
            AbstractSyntaxTree itsAbstractSyntaxTree = null;
            if (_ts.Peek() == Constants.ID) {
                Token tid = Expect(Constants.ID);
                Expect(Constants.ASSIGN);
                AbstractSyntaxTree val = Value();
                Computing expr = Expression();
                if (expr == null) itsAbstractSyntaxTree = new Assignment(tid.Value, val);
                else {
                    expr.ChildLeft = val;
                    itsAbstractSyntaxTree = new Assignment(tid.Value, expr);
                }
            } else if (_ts.Peek() == Constants.PRINT) {
                Expect(Constants.PRINT);
                Token tid = Expect(Constants.ID);
                itsAbstractSyntaxTree = new Printing(tid.Value);
            } else Error("expected id or print");

            return itsAbstractSyntaxTree;
        }

        private Computing Expression() {
            Operation operation = Operation.NONE;

            switch (_ts.Peek()) {
                case Constants.PLUS:
                    Expect(Constants.PLUS);
                    operation = Operation.PLUS;
                    break;
                case Constants.MINUS:
                    Expect(Constants.MINUS);
                    operation = Operation.MINUS;
                    break;
                case Constants.MULTIPLY:
                    Expect(Constants.MULTIPLY);
                    operation = Operation.MULTIPLY;
                    break;
                case Constants.EXPONENT:
                    Expect(Constants.EXPONENT);
                    operation = Operation.EXPONENT;
                    break;
                case Constants.MODULO:
                    Expect(Constants.MODULO);
                    operation = Operation.MODULO;
                    break;
                case Constants.DIVIDE:
                    Expect(Constants.DIVIDE);
                    operation = Operation.DIVIDE;
                    break;
                case Constants.EOF:
                    break;
                case Constants.PRINT:
                    break;
                case Constants.ID:
                    break;
            }

            if (operation == Operation.NONE) {
                // Do nothing for lambda-production
                return null;
            }

            AbstractSyntaxTree val = Value();
            Computing expr = Expression();
            Computing itsAst = expr != null
                ? new Computing(operation, null, expr)
                : new Computing(operation, null, val);
            if (expr != null) {
                expr.ChildLeft = val;
            }

            return itsAst;
        }

        private AbstractSyntaxTree Value() {
            AbstractSyntaxTree itsAbstractSyntaxTree = null;
            if (_ts.Peek() == Constants.ID) {
                Token tid = Expect(Constants.ID);
                itsAbstractSyntaxTree = new SymReferencing(tid.Value);
            } else if (_ts.Peek() == Constants.INUM) {
                Token tid = Expect(Constants.INUM);
                itsAbstractSyntaxTree = new IntConstant(tid.Value);
            } else if (_ts.Peek() == Constants.FNUM) {
                Token tid = Expect(Constants.FNUM);
                itsAbstractSyntaxTree = new FloatConstant(tid.Value);
            } else Error("expected id, int, or float got: " + _ts.Peek());

            return itsAbstractSyntaxTree;
        }

        private Token Expect(Constants type) {
            Token t = _ts.Advance();
            if (t.Type != type) {
                throw new Exception("Expected type " + type + " but received type " + t.Type);
            }

            return t;
        }

        private static void Error(string message) {
            throw new Exception(message);
        }
    }
}