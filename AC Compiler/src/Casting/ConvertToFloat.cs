using System;
using ACCompiler.Types;

namespace ACCompiler.Casting {
    public class ConvertToFloat : AbstractSyntaxTree {
        private readonly AbstractSyntaxTree _child;

        public ConvertToFloat(AbstractSyntaxTree child) {
            _child = child;
        }


        public override void PrettyPrint() {
            Console.WriteLine(" i2f ");
        }

        public override void SymbolTableFilling() {
            _child.SymbolTableFilling();
        }

        public override void TypeChecking() {
            _child.TypeChecking();
            Type = Constants.FNUM;
        }

        public override void CodeGeneration() {
            _child.CodeGeneration();
            Emit(" 5 k ");
        }
    }
}