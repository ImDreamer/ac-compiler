using System;
using ACCompiler.Types;

namespace ACCompiler {
    public class IntConstant : AbstractSyntaxTree {
        private readonly string _val;

        public IntConstant(string v) {
            _val = v;
        }


        public override void PrettyPrint() {
            Console.WriteLine(_val);
        }

        public override void SymbolTableFilling() { }

        public override void TypeChecking() {
            Type = Constants.INUM;
        }

        public override void CodeGeneration() {
            Emit(" " + _val + " ");
        }
    }
}