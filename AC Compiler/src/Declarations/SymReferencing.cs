using System;
using ACCompiler.Types;

namespace ACCompiler.Declarations {
    public class SymReferencing : AbstractSyntaxTree {
        private readonly string _id;

        public SymReferencing(string i) {
            _id = i;
        }


        public override void PrettyPrint() {
            Console.WriteLine(_id);
        }

        public override void SymbolTableFilling() {
        }

        public override void TypeChecking() {
            Type = SymbolTable[_id];
        }

        public override void CodeGeneration() {
            Emit("l");
            Emit(_id + " ");
        }
    }
}