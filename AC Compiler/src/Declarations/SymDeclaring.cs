using ACCompiler.Types;

namespace ACCompiler.Declarations {
    public class SymDeclaring : AbstractSyntaxTree {
        protected string Id;


        public override void PrettyPrint() { }

        public override void SymbolTableFilling() { }

        public override void TypeChecking() { }

        public override void CodeGeneration() { }
    }
}