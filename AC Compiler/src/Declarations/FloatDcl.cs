using System;

namespace ACCompiler.Declarations {
    public class FloatDcl : SymDeclaring {
        public FloatDcl(string id) {
            Id = id;
        }


        public override void PrettyPrint() {
            Console.WriteLine("float " + Id);
        }

        public override void SymbolTableFilling() {
            if (!SymbolTable.ContainsKey(Id)) SymbolTable.Add(Id, Constants.FLOATDCL);
            else Error("variable " + Id + " is already declared");
        }

        public override void TypeChecking() { }

        public override void CodeGeneration() { }
    }
}