using System;

namespace ACCompiler.Declarations {
    public class IntDcl : SymDeclaring {
        public IntDcl(string i) {
            Id = i;
        }

        public override void PrettyPrint() {
            Console.WriteLine("int " + Id);
        }

        public override void SymbolTableFilling() {
            if (!SymbolTable.ContainsKey(Id)) SymbolTable.Add(Id, Constants.INTDCL);
            else Error("variable " + Id + " is already declared");
        }

        public override void TypeChecking() { }

        public override void CodeGeneration() { }
    }
}