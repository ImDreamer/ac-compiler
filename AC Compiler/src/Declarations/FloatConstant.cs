using System;
using ACCompiler.Types;

namespace ACCompiler {
    public class FloatConstant : AbstractSyntaxTree {
        private readonly string _val;

        public FloatConstant(string v) {
            _val = v;
        }


        public override void PrettyPrint() {
            Console.WriteLine(_val);
        }

        public override void SymbolTableFilling() { }

        public override void TypeChecking() {
            Type = Constants.FNUM;
        }

        public override void CodeGeneration() {
            Emit(" " + _val + " ");
        }
    }
}