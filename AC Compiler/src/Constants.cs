// ReSharper disable InconsistentNaming

namespace ACCompiler {
    public enum Constants {
        EOF,
        PLUS,
        MINUS,
        MULTIPLY,
        EXPONENT,
        DIVIDE,
        MODULO,
        FLOATDCL,
        INTDCL,
        PRINT,
        ASSIGN,
        INUM,
        FNUM,
        ID
    }

    public enum Operation {
        NONE = ' ',
        PLUS = '+',
        MINUS = '-',
        MULTIPLY = '*',
        EXPONENT = '^',
        MODULO = '%',
        DIVIDE = '/'
    }
}