using System;
using ACCompiler.Types;
using ACCompiler.Util;

namespace ACCompiler {
    public static class Scanner {
        private static CharStream _stream;

        public static void Init(CharStream s) {
            _stream = s;
        }

        public static Token Scan() {
            Token ans;
            while (char.IsWhiteSpace(_stream.Peek()))
                _stream.Advance();
            if (_stream.Eof())
                ans = new Token(Constants.EOF);
            else {
                if (IsDigit(_stream.Peek()))
                    ans = ScanDigits();
                else {
                    char ch = _stream.Advance();

                    switch (RepresentativeChar(ch)) {
                        case 'a': // matches {a, b, ..., z} - {f, i, p}
                            ans = new Token(Constants.ID, "" + ch);
                            break;
                        case 'f':
                            if (_stream.Peek() == 'l') {
                                _stream.Advance();
                                if (_stream.Peek() == 'o') {
                                    _stream.Advance();
                                    if (_stream.Peek() == 'a') {
                                        _stream.Advance();
                                        if (_stream.Peek() == 't') {
                                            _stream.Advance();
                                            ans = new Token(Constants.FLOATDCL);
                                            break;
                                        }
                                    }
                                } else {
                                    throw new Exception("Lexical error on character:" + ch);
                                }
                            }

                            ans = new Token(Constants.FLOATDCL);
                            break;
                        case 'i':
                            if (_stream.Peek() == 'n') {
                                _stream.Advance();
                                if (_stream.Peek() != 't')
                                    throw new Exception("Lexical error on character: " + ch);
                                _stream.Advance();
                                ans = new Token(Constants.INTDCL);
                                break;
                            }

                            ans = new Token(Constants.INTDCL);
                            break;
                        case 'p':
                            ans = new Token(Constants.PRINT);
                            break;
                        case '=':
                            ans = new Token(Constants.ASSIGN);
                            break;
                        case '+':
                            ans = new Token(Constants.PLUS);
                            break;
                        case '-':
                            ans = new Token(Constants.MINUS);
                            break;
                        case '*':
                            ans = new Token(Constants.MULTIPLY);
                            break;
                        case '^':
                            ans = new Token(Constants.EXPONENT);
                            break;
                        case '%':
                            ans = new Token(Constants.MODULO);
                            break;
                        case '/':
                            ans = new Token(Constants.DIVIDE);
                            break;
                        default:
                            throw new Exception("Lexical error on character: " + ch + " at column: " + _stream.Column);
                    }
                }
            }

            return ans;
        }

        private static Token ScanDigits() {
            var val = "";
            Constants type;
            while (IsDigit(_stream.Peek())) {
                val = val + _stream.Advance();
            }

            if (_stream.Peek() != '.')
                type = Constants.INUM;
            else {
                type = Constants.FNUM;
                val = val + _stream.Advance();
                while (IsDigit(_stream.Peek())) {
                    val = val + _stream.Advance();
                }
            }

            return new Token(type, val);
        }

        private static char RepresentativeChar(char ch) {
            if (
                'a' <= ch && ch <= 'z'
                          && ch != 'f'
                          && ch != 'i'
                          && ch != 'p'
            )
                return 'a';
            return ch;
        }

        private static bool IsDigit(char ch) {
            return '0' <= ch && ch <= '9';
        }
    }
}