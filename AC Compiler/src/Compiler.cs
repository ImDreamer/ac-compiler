﻿using System;
using System.IO;
using ACCompiler.Types;
using ACCompiler.Util;

namespace ACCompiler {
    public static class Compiler {
        public static string Compile(string input, bool verbose = false) {
            if (verbose)
                Console.WriteLine("Now trying: " + input);
            Parser parser = new Parser(new CharStream(new StreamReader(GenerateStreamFromString(input))));
            AbstractSyntaxTree abstractSyntaxTree = parser.Program();
            if (verbose) {
                Console.WriteLine("   Parse successful");
                abstractSyntaxTree.PrettyPrint();
                Console.WriteLine(" Pretty Printing successful");
            }

            abstractSyntaxTree.SymbolTableFilling();
            if (verbose) {
                Console.WriteLine(" Symbol Table filling successful");
                foreach ((string key, Constants value) in AbstractSyntaxTree.SymbolTable) {
                    Console.WriteLine("Key = {0}, Value = {1}", key, value);
                }
            }

            abstractSyntaxTree.TypeChecking();
            if (verbose) {
                Console.WriteLine(" Type Checking successful");
                abstractSyntaxTree.PrettyPrint();
                Console.WriteLine(" Pretty Printing successful");
            }

            abstractSyntaxTree.CodeGeneration();
            if (verbose) {
                Console.WriteLine(AbstractSyntaxTree.Code);
                Console.WriteLine(" Code Generation successful");
            }

            string code = AbstractSyntaxTree.Code;
            AbstractSyntaxTree.Code = "";
            AbstractSyntaxTree.SymbolTable.Clear();
            return code;
        }

        public static void WriteToFile(string input, bool verbose, string path) {
            FileInfo file = new FileInfo(path);
            file.Directory?.Create();
            File.WriteAllText(file.FullName, Compile(input, verbose));
        }

        public static Stream GenerateStreamFromString(string s) {
            MemoryStream stream = new MemoryStream();
            StreamWriter writer = new StreamWriter(stream);
            writer.Write(s);
            writer.Flush();
            stream.Position = 0;
            return stream;
        }
    }
}