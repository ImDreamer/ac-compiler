using System;
using System.Diagnostics;
using System.IO;
using System.Linq;

namespace ACCompiler {
    public static class Start {
        public static void Main(string[] args) {
            if (args.Length == 0) {
                Console.WriteLine("Sorry, you need to input string");
                return;
            }

            if (args[0] == "-help") {
                Console.WriteLine("-x \t\tExecutes when compiled if dc is present in the out/ folder.\n" +
                                  "-f \"path\" \tLoads the source from a file.\n" +
                                  "-o \"path\" \tWrites the output to the specified file.\n" +
                                  "-v \t\tEnable logging.\n" +
                                  "-help \t\tShows this.");
                return;
            }

            string input = args[0];
            var arguments = args.ToList();
            var verbose = false;
            var path = "";
            if (input.Equals("-")) {
                Console.WriteLine("Malformed input.");
                return;
            }

            if (arguments.Contains("-v")) {
                verbose = true;
            }

            if (arguments.Contains("-f")) {
                int index = arguments.IndexOf("-f");
                if (arguments.Count - 1 >= index + 1) {
                    input = File.ReadAllText(arguments[index + 1]);
                } else {
                    Console.WriteLine("No path specified\nUsing default");
                }
            }

            if (arguments.Contains("-o")) {
                int index = arguments.IndexOf("-o");
                if (arguments.Count - 1 >= index + 1) {
                    path = arguments[index + 1];
                } else {
                    Console.WriteLine("No path specified\nUsing default");
                }
            }

            if (arguments[0] == "-v" || arguments[0] == "-o") {
                Console.WriteLine("Nothing done, no input or input is not the first argument");
                return;
            }

            if (path != string.Empty) {
                Compiler.WriteToFile(input, verbose, path);
                if (!arguments.Contains("-x")) return;
                if (!File.Exists("out/dc.exe")) {
                    Console.WriteLine("dc not found");
                    return;
                }

                Directory.SetCurrentDirectory("out/");
                ProcessStartInfo startInfo = new ProcessStartInfo {
                    Arguments = "-f " + path, FileName = "dc.exe"
                };
                Console.WriteLine("Running the code: " + File.ReadAllText(path) + "\nResult:");
                Process.Start(startInfo);
            } else {
                Compiler.WriteToFile(input, verbose, "out/code.dc");
                if (!arguments.Contains("-x")) return;
                if (!File.Exists("out/dc.exe")) {
                    Console.WriteLine("dc not found");
                    return;
                }

                Directory.SetCurrentDirectory("out/");
                ProcessStartInfo startInfo = new ProcessStartInfo {
                    Arguments = "-f code.dc", FileName = "dc"
                };
                Console.WriteLine("Running the code: " + File.ReadAllText("code.dc") + "\nResult:");
                Process.Start(startInfo);
            }
        }
    }
}