using System;
using ACCompiler.Types;

namespace ACCompiler.Operations {
    public class Assignment : AbstractSyntaxTree {
        private readonly string _id;
        private AbstractSyntaxTree _child;

        public Assignment(string id, AbstractSyntaxTree child) {
            _id = id;
            _child = child;
        }


        public override void PrettyPrint() {
            Console.WriteLine(_id + " = " + _child);
            _child.PrettyPrint();
        }

        public override void SymbolTableFilling() {
            _child.SymbolTableFilling();
        }

        public override void TypeChecking() {
            _child.TypeChecking();
            Constants m = SymbolTable[_id];
            Constants t = Generalize(_child.Type, m);
            _child = Convert(_child, m);
            Type = t;
        }

        public override void CodeGeneration() {
            _child.CodeGeneration();
            Emit(" s");
            Emit(_id);
            Emit(" 0 k ");
        }
    }
}