using System;
using ACCompiler.Types;

namespace ACCompiler.Operations {
    public class Printing : AbstractSyntaxTree {
        private readonly string _id;

        public Printing(string id) {
            _id = id;
        }


        public override void PrettyPrint() {
            Console.WriteLine("print " + _id);
        }

        public override void SymbolTableFilling() { }

        public override void TypeChecking() { }

        public override void CodeGeneration() {
            Emit("l");
            Emit(_id);
            Emit(" p ");
            Emit("si ");
        }
    }
}