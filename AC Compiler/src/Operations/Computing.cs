using System;
using ACCompiler.Types;

namespace ACCompiler.Operations {
    public class Computing : AbstractSyntaxTree {
        private readonly Operation _operation;
        public AbstractSyntaxTree ChildLeft;
        public AbstractSyntaxTree ChildRight;

        public Computing(Operation op, AbstractSyntaxTree ch1, AbstractSyntaxTree ch2) {
            ChildLeft = ch1;
            ChildRight = ch2;
            _operation = op;
        }


        public override void PrettyPrint() {
            ChildLeft.PrettyPrint();
            Console.WriteLine(_operation);
            ChildRight.PrettyPrint();
        }

        public override void SymbolTableFilling() {
            ChildLeft.SymbolTableFilling();
            ChildRight.SymbolTableFilling();
        }

        public override void TypeChecking() {
            ChildLeft.TypeChecking();
            ChildRight.TypeChecking();
            Constants m = Generalize(ChildLeft.Type, ChildRight.Type);
            ChildLeft = Convert(ChildLeft, m);
            ChildRight = Convert(ChildRight, m);
            Type = m;
        }

        public override void CodeGeneration() {
            ChildLeft.CodeGeneration();
            ChildRight.CodeGeneration();
            Emit(((char) _operation).ToString());
        }
    }
}