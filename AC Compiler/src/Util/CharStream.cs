using System.IO;

namespace ACCompiler.Util {
    public class CharStream {
        private readonly StreamReader _reader;
        public int Column;
        private char _nextChar;
        private bool _eof;

        public CharStream(StreamReader ds) {
            _reader = ds;
            _eof = false;
            _nextChar = (char) 0;
            Advance();
        }

        public char Peek() {
            return _nextChar;
        }

        public bool Eof() {
            return _eof;
        }

        public char Advance() {
            char ans = _nextChar;
            int next = _reader.Read();
            if (next == -1) {
                _eof = true;
                _nextChar = (char) 0;
            } else {
                Column++;
                _nextChar = (char) next;
            }

            return ans;
        }
    }
}