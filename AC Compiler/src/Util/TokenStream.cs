using ACCompiler.Types;

namespace ACCompiler.Util {
    public class TokenStream {
        private Token _nextToken;

        public TokenStream(CharStream s) {
            Scanner.Init(s);
            Advance();
        }

        public Constants Peek() {
            return _nextToken.Type;
        }

        public Token Advance() {
            Token ans = _nextToken;
            _nextToken = Scanner.Scan();
            return ans;
        }
    }
}